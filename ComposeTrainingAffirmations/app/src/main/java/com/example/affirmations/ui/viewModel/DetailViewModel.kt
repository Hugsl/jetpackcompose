package com.example.affirmations.ui.viewModel

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update


class DetailViewModel : ViewModel(){



   private val _uiState = MutableStateFlow<Affirmation?>(null)

    val uiState:StateFlow<Affirmation?> = _uiState.asStateFlow()
    val list = Datasource().loadAffirmations()






    fun loadAffirmation(id: Int){

            val aff =list.firstOrNull(){it.ID == id}
            _uiState.value=aff


        }


    fun sendMail(to:String, subject: String,context: Context){
        try {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "message/rfc822"
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
            intent.putExtra(Intent.EXTRA_SUBJECT,subject)
            context.startActivity(intent)
        }
        catch (e: ActivityNotFoundException){

        }
    }
    fun dial(phone:String,context: Context){

        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel",phone,null))
        context.startActivity(intent)
    }


}

data class DetUiState(
    val ID: Int =0,
    @StringRes val stringResourceId: Int = 0,
    @DrawableRes val imageResourceId: Int = 0,
    val description: String = "",
    val telephoneNumber : String = "",
    val mail : String=""

)



