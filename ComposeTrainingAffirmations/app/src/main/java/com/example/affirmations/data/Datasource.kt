/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations.data
import com.example.affirmations.R
import com.example.affirmations.model.Affirmation
/**
 * [Datasource] generates a list of [Affirmation]
 */
class Datasource() {
    public fun loadAffirmations(): List<Affirmation> {
        return listOf<Affirmation>(
            Affirmation(10,R.string.affirmation1, R.drawable.image1, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nibh nisl, auctor vitae suscipit eu, posuere in turpis. Curabitur bibendum ullamcorper neque. Duis vel purus vel libero pretium posuere sed non nisl.","111111111","hugo.hugo@hugo.com"),
            Affirmation(11,R.string.affirmation2, R.drawable.image2,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nibh nisl, auctor vitae suscipit eu, posuere in turpis. Curabitur bibendum ullamcorper neque. Duis vel purus vel libero pretium posuere sed non nisl.","222222222","hugo.hugo@hugo.com"),
            Affirmation(21,R.string.affirmation3, R.drawable.image3,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nibh nisl, auctor vitae suscipit eu, posuere in turpis. Curabitur bibendum ullamcorper neque. Duis vel purus vel libero pretium posuere sed non nisl.","333333333","hugo.hugo@hugo.com"),
            Affirmation(32,R.string.affirmation4, R.drawable.image4,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nibh nisl, auctor vitae suscipit eu, posuere in turpis. Curabitur bibendum ullamcorper neque. Duis vel purus vel libero pretium posuere sed non nisl.","444444444","hugo.hugo@hugo.com"),
            Affirmation(46,R.string.affirmation5, R.drawable.image5,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nibh nisl, auctor vitae suscipit eu, posuere in turpis. Curabitur bibendum ullamcorper neque. Duis vel purus vel libero pretium posuere sed non nisl.","555555555","hugo.hugo@hugo.com"),
            Affirmation(54,R.string.affirmation6, R.drawable.image6,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nibh nisl, auctor vitae suscipit eu, posuere in turpis. Curabitur bibendum ullamcorper neque. Duis vel purus vel libero pretium posuere sed non nisl.","666666666","hugo.hugo@hugo.com"),
            Affirmation(68,R.string.affirmation7, R.drawable.image7,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nibh nisl, auctor vitae suscipit eu, posuere in turpis. Curabitur bibendum ullamcorper neque. Duis vel purus vel libero pretium posuere sed non nisl.","777777777","hugo.hugo@hugo.com"),
            Affirmation(47,R.string.affirmation8, R.drawable.image8,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nibh nisl, auctor vitae suscipit eu, posuere in turpis. Curabitur bibendum ullamcorper neque. Duis vel purus vel libero pretium posuere sed non nisl.","888888888","hugo.hugo@hugo.com"),
            Affirmation(83,R.string.affirmation9, R.drawable.image9,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nibh nisl, auctor vitae suscipit eu, posuere in turpis. Curabitur bibendum ullamcorper neque. Duis vel purus vel libero pretium posuere sed non nisl.","999999999","hugo.hugo@hugo.com"),
            Affirmation(49,R.string.affirmation10, R.drawable.image10,"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nibh nisl, auctor vitae suscipit eu, posuere in turpis. Curabitur bibendum ullamcorper neque. Duis vel purus vel libero pretium posuere sed non nisl.","123581321","hugo.hugo@hugo.com"))
    }
}
