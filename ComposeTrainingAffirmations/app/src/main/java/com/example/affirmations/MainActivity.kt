/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import androidx.compose.material.Icon
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.*
import androidx.compose.animation.animateContentSize
import  androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextOverflow
import com.example.affirmations.ui.viewModel.MainViewModel

const val Tag = "main act";
class MainActivity : ComponentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

            // TODO 5. Show screen
            AffirmationApp()
        }
    }
}

@Composable
fun AffirmationApp() {
    // TODO 4. Apply Theme and affirmation list
    AffirmationsTheme() {

       Column() {
        AffirmationList(affirmationList = Datasource().loadAffirmations())
       }


    }


}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>, modifier: Modifier = Modifier) {
    // TODO 3. Wrap affirmation card in a lazy column


    LazyColumn(contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp)) {

        items(affirmationList) { item: Affirmation -> AffirmationCard(affirmation = item) }

    }
}

@Composable
fun AffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
    // TODO 1. Your card UI
    var expanded by remember { mutableStateOf(false) }

    val color by animateColorAsState(
        targetValue = if (expanded) Color.Transparent else MaterialTheme.colors.surface,
    )
    Card(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth()
    ) {
        Column(modifier = Modifier
            .padding(start = 16.dp, top = 8.dp, bottom = 16.dp, end = 16.dp)
            .animateContentSize(
                animationSpec = spring(
                    dampingRatio = Spring.DampingRatioMediumBouncy,
                    stiffness = Spring.StiffnessLow
                )
            )
            .background(color = color)) {
            Row() {
                Image(
                    painter = painterResource(id = affirmation.imageResourceId),
                    contentDescription = affirmation.stringResourceId.toString()
                )
                Text(
                    stringResource(id = affirmation.stringResourceId),
                    modifier = Modifier
                        .padding(start = 10.dp)
                        .weight(1f)
                )
                IconButton(onClick = {
                    expanded = !expanded
                }) {
                    Icon(
                        imageVector = if (expanded) Icons.Filled.KeyboardArrowUp else Icons.Filled.KeyboardArrowDown,
                        tint = MaterialTheme.colors.secondary,
                        contentDescription = "Hell nah"
                    )
                }
            }
            if (expanded) AffirmationText2(text = affirmation.description,id=affirmation.ID)
        }
    }

}

@Composable
private fun AffirmationText2(text: String,id:Int) {
    val context = LocalContext.current
    val  MaViMo= MainViewModel()
    Card(modifier = Modifier.fillMaxWidth()) {
        Column(

        ) {
            Text(
                text = "TEXT",
                style = MaterialTheme.typography.h3,
            )
            Text(
                text = text,
                style = MaterialTheme.typography.body1, overflow = TextOverflow.Ellipsis, maxLines = 2
            )

            TextButton(onClick = {
                MaViMo.intentIDToDetail(id = id,context)

            }) {

                Text(text = "Watch more")
            }
        }
    }


}
@Composable
private fun ByPass(id:Int){

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

