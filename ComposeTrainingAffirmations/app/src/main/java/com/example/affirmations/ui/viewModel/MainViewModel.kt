package com.example.affirmations.ui.viewModel

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.example.affirmations.DetailActivity
import com.example.affirmations.MainActivity

class MainViewModel : ViewModel() {

fun intentIDToDetail(id:Int,context: Context ){
val intent = Intent(context,DetailActivity::class.java)
    intent.putExtra("ID",id)
    context.startActivity(intent)
}

}