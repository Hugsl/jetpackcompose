package com.example.affirmations


import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.affirmations.ui.viewModel.DetailViewModel


class DetailActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

            Detail()
        }
    }

    @Preview
    @Composable
    fun Detail(detVieMod: DetailViewModel = DetailViewModel()) {
        val bundle: Bundle? = intent.extras
        val ID = intent.getIntExtra("ID", 0)
        val detailModel by detVieMod.uiState.collectAsState()
        detVieMod.loadAffirmation(ID)
        val context = LocalContext.current

        val BackgroundColor= Color( red = 238, green = 208, blue=198)

        detailModel?.let {
            Card(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth(), backgroundColor = BackgroundColor,

            ) {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(15.dp)
                     ,
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = stringResource(id = detailModel!!.stringResourceId),
                        style = MaterialTheme.typography.h4,
                        modifier = Modifier.weight(1f)



                    )
                    Image(
                        painter = painterResource(id = detailModel!!.imageResourceId),
                        contentDescription = "",
                        modifier = Modifier.fillMaxWidth().size(250.dp)
                    )
                    Text(text = detailModel!!.description, style = MaterialTheme.typography.body1, modifier = Modifier.weight(1f)

                    )

                    Row(modifier = Modifier.fillMaxWidth().padding(15.dp)) {
                        Button(
                            onClick = {
                                detVieMod.sendMail(
                                    detailModel!!.mail,
                                    "",
                                    context = context
                                )
                            },
                            modifier = Modifier
                                .padding(start = 10.dp)
                                .weight(1f)
                        ) {
                            Text(text = "Mail!")
                        }
                        Button(
                            onClick = {
                                detVieMod.dial(
                                    detailModel!!.telephoneNumber,
                                    context = context
                                )
                            }, modifier = Modifier
                                .padding(start = 10.dp, end = 10.dp)
                                .weight(1f)
                        ) {
                            Text(text = "Call Me!")
                        }
                    }

                }
            }

        }


    }


}